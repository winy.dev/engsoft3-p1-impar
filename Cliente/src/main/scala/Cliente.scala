import akka.actor._;
import scala.collection.mutable._;
import scala.concurrent.duration._;
import scala.io._;
import java.io.File;
import java.io.FileWriter;


/** Cliente
 * 
 * Seu objetivo é receber os dados, enviá-los para o servidor e gravar o resultado em arquivo.
 */
class Client extends Actor{
    /* Os servidores */
    private val servidor1:ActorSelection = context.actorSelection("akka.tcp://Servidor@127.0.0.1:8080/user/servidor");
    private val servidor2:ActorSelection = context.actorSelection("akka.tcp://Servidor@127.0.0.1:8081/user/servidor");

    private var users:Map[String,User] = Map[String,User](); //Lista de usuários
    private var counter:Int = 0;
    private var fim:Int = 0;
    private var tempoInicial:Long = System.currentTimeMillis;
    private var len:Int = 0; //Tamanho do array de dados
    private var tam:Int = 3500; //Tamanho do subarray a ser enviado para o servidor

    /** Grava os dados nos devidos arquivos. */
    def gravaDados():Unit = {
        /* Arquivo que contem os dados de gastos de cpu e memória */
        val fileDados:FileWriter = new FileWriter(new File("out/dados"));
        /* Arquivo que contem os dados de tempo gasto */
        val fileTempo:FileWriter = new FileWriter(new File("out/tempo"));
        var cpu:Double = 0.0; //Armazena o total gasto de cpu
        var mem:Double = 0.0; //Armazena o total gasto de memória
        var tmp:Int = 0; //Armazena o total gasto de tempo

        /* Percorre a lista de usuários... */
        users.values.foreach( user => {
            /* Arquivo de cada user. Contem os pids */
            val fileUsers = new FileWriter(new File("out/"+user.username));
            /* Arquivo com todos os usuários e seus respectivos pids */
            val filePID = new FileWriter(new File("out/pid"));

            /* Percorre a lista de processos... */
            user.processos.foreach( proc => {
                fileUsers.write("pid: %s\n".format(proc.pid));
                filePID.write("user: %s pid: %s\n".format(user.username,proc.pid));
            });

            fileUsers.close();
            filePID.close();

            /* Soma aos valores totais dos gastos */
            cpu += user.gastoCPU;
            mem += user.gastoMEMORIA;
            tmp += user.gastoTEMPO;

            /* Calcula as médias */
            val calc = user.calcularMedias();

            fileDados.write("user: %s \tmemória: %1.2f \tcpu: %1.2f\n".format(user.username,calc._2,calc._1));
            fileTempo.write("user: %s \ttempo: %d seg\n".format(user.username,calc._3));
        });

        fileDados.write("Memória total: %1.2f \tCPU total: %1.2f\n".format(mem,cpu));
        fileTempo.write("Tempo total: %d seg\n".format(tmp));
        fileDados.close();
        fileTempo.close();
    }

    def receive: Receive = {
        case Start(conteudo) => {
            println("Ola!");
            var arr:Array[String] = conteudo.toArray;
            len = arr.length;
            fim = (len/tam).toInt;
            for(i <- 0 until len by (tam*2)){
                servidor1 ! Processa(arr.slice(i,i+tam)); 
                servidor2 ! Processa(arr.slice(i+tam,i+(tam*2)));
            }
        }

        case Result(lista) => {
            counter += 1;

            /* Percorre o resultado recebido... */
            lista.values.foreach( user => {
                /* Verifica se o usuário ainda não está na lista de usuários */
                if(!users.contains(user.username)){
                    users(user.username) = new User(user.username);
                }

                /* Acrescenta o resultado ao user que está na lista */
                users(user.username).add(user);
            });

            /* Verifica se chegou ao final, exibe o tempo gasto no processamento
             * Grava os dados e finaliza o sistema de atores. */
            if(counter == fim){
                /* Exibe o tempo gasto no processamento */
                println("Tempo de processamento dos dados: "+ (System.currentTimeMillis-tempoInicial).millis);

                tempoInicial = System.currentTimeMillis;
                gravaDados();
                println("Tempo de gravação: "+ (System.currentTimeMillis-tempoInicial).millis);

                println("Número total de processos: "+len);

                /* Finaliza o sistema de atores */
                context.system.shutdown;
            }

        }
      
    }
}

object Cliente {
    def main(args: Array[String]): Unit = {

        /* Diretório com os resultados */
        val out = new File("out");
        if (!out.exists()) out.mkdir();
        out.listFiles().foreach(_.delete());

        /* Abre o arquivo com os dados */
        val arquivo = Source.fromFile("src/main/resources/pimpar");
        val conteudo = arquivo.getLines.toArray;

        /* Inicia o sistema de atores */
        val system = ActorSystem("Cliente");
        val cliente = system.actorOf(Props[Client], "cliente");
        cliente ! Start(conteudo);

        /* Fecha o arquivo com os dados originais */
        arquivo.close();

    }
}
