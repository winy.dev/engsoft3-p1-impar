import java.io.Serializable;

/** Processo do usuário
* 
* @param _pid Identificação do processo
* @param _cpu Quantidade de CPU gasta
* @param _mem Porcentagem de memória gasta
* @param _tempo Tempo gasto
*/
@SerialVersionUID(100L)
class Processo(
    private[this] var _pid: String,
    private[this] var _cpu: String,
    private[this] var _memoria: String,
    private[this] var _tempo: String) extends Serializable {

    /* Gettters/Setters */
    def pid:String = _pid;
    def cpu:String = _cpu;
    def memoria:String = _memoria;
    def tempo:String = _tempo;
    def pid_=(value:String){ _pid = value; }
    def cpu_=(value:String){ _cpu = value; }
    def memoria_=(value:String){ _memoria = value; }
    def tempo_=(value:String){ _tempo = value; }

}


/** Usuário e seus processos.
 *
 *  @constructor cria um novo usuario.
 *  @param _user Nome do usuário
 *  @param _processos Lista de processos do usuário
 */
@SerialVersionUID(100L)
class User (
    private[this] var _username: String, 
    private[this] var _processos: Array[Processo]) extends Serializable {

    /* Total gasto */
    private[this] var cpuGasta:Double = 0.0;
    private[this] var memGasta:Double = 0.0;
    private[this] var tempoGasto:Int = 0;

    /** Usuário inicialmente sem processos.
     *
     *  @constructor cria um novo usuario.
     *  @param _user Nome do usuário
     */
    def this(_username: String)={
        this(_username, new Array[Processo](0));
    }

    /* Gettters/Setters */
    def username:String = _username;
    def processos:Array[Processo] = _processos;
    def gastoCPU:Double = cpuGasta;
    def gastoMEMORIA:Double = memGasta;
    def gastoTEMPO:Int = tempoGasto;
    def username_=(value:String){ _username = value; }

    /** Adiciona um processo a lista de processos
     *  @param proc O processo a ser adicionado
     */
    def addProcesso(proc: Processo):Unit={
        _processos = proc +: _processos;
        addGastos(proc.cpu.toDouble,proc.memoria.toDouble,Utils.converteTempo(proc.tempo));
    }

    /** Adiciona um novo processo a lista de processos
     * @param pid Identificação do processo
     * @param cpu Quantidade de CPU gasta
     * @param mem Porcentagem de memória gasta
     * @param tempo Tempo gasto
     */
    def addProcesso(pid: String, cpu: String, mem: String, tempo: String):Unit={
        addProcesso(new Processo(pid,cpu,mem,tempo));
    }

    /** Acrescenta ao user atual os dados de outro user
     *  @param user O usuario a ser juntado
     */
    def add(user:User):Unit={
        _processos = user.processos ++ _processos;
        addGastos(user.gastoCPU,user.gastoMEMORIA,user.gastoTEMPO);
    }

    /** Adiciona quantidades gastas pelo processo
     * @param cpu Quantidade de CPU gasta
     * @param mem Porcentagem de memória gasta
     * @param tempo Tempo gasto
     */
    def addGastos(cpu: Double, mem: Double, tempo: Int):Unit={
        cpuGasta += cpu;
        memGasta += mem;
        tempoGasto += tempo;
    }

    /** Calcula as médias do que foi gasto pelos processos
     */
    def calcularMedias():(Double,Double,Int)={
        val l = processos.length;
        var cg = 0.0;
        var mg = 0.0;
        var tg = 0;
        if(l > 0){
            cg = cpuGasta/l;
            mg = memGasta/l;
            tg = (tempoGasto/l).toInt;
        }
        return (cg,mg,tg);
    }
    
}