import akka.actor._;
import scala.collection.mutable._;


/** Cliente
 * 
 * Seu objetivo é receber os dados de um cliente e devolver o resultado.
 */
class Server extends Actor {

    def receive: Receive = {

        case Start => {
        	println("Servidor iniciado");
        }

        case Processa(conteudo) => {
        	/* Lista dos usuários */
        	var users = Map[String,User]();

        	/* Percorre o conteúdo recebido... */
            conteudo.foreach( linha=> {
                val proc = linha.split(" ");
                val processo = new Processo(proc(1),proc(2),proc(3),proc(4));
                /* Verifica se o usuário ainda não está na lista de usuários */
                if(!users.contains(proc(0))){
                    users(proc(0)) = new User(proc(0));
                }
                users(proc(0)).addProcesso(processo);
            });

            /* Devolve o resultado */
            sender ! Result(users);
        }
    }

}

object Servidor {
    def main(args: Array[String]): Unit = {
	    val system = ActorSystem("Servidor");
	    val servidor = system.actorOf(Props[Server], "servidor");
	    servidor ! Start;
    }
}
