/** Fornece funções uteis */
object Utils {

	/** Converte o tempo que é recebido por uma string para segundos.
     *
     *  @param _tempo O tempo no formato min:seg.
     *  @return o tempo em segundos.
     */
	def converteTempo(_tempo: String):Int={
		var tempo = _tempo.split(":");
		return (tempo(0).toInt * 60) + tempo(1).toInt;
	}

}