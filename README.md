# Prova 1 de Engenharia de Software

Projeto desenvolvido para a disciplina de Engenharia de Software III, ministrada pelo Profº Me. Alexandre Garcia, do Curso Superior de Tecnologia em Análise e Desenvolvimento de Sistemas, da
Faculdade de Tecnologia da Baixada Santista, 4º ciclo matutino.

## Projeto
Consiste em ler um arquivo contendo dados de processos de usuário[^1] e criar ao total 11 arquivos.

**Arquivos a serem criados:**
1. Um arquivo para cada usuário contendo a lista de processos executados por esse usuário (8 arquivos)
2. Um arquivo com as médias do tempo gasto no total e por usuário (1 arquivo)
3. Um arquivo com as médias de memória e cpu gastos ńo total e por usuário (1 arquivo)
4. Um arquivo com os usuários e seus respectivos pids (1 arquivo)  

**Estrutura do arquivo *pimpar*:**

| user     | pid   | cpu   | mem   | tempo |
| :------: | :---: | :---: | :---: | :---: |
| mysql    | 85097 | 87.8  | 85.8  | 93:60 |
| java     | 6911  | 36.1  | 25.7  | 16:39 |
| postgres | 89140 | 42.4  | 13.0  | 47:15 |
| apache   | 45577 | 31.6  | 17.8  | 87:07 |
| docker   | 79463 | 38.2  | 44.9  | 87:03 |
| mysql    | 77535 | 79.3  | 26.5  | 28:44 |
| root     | 35312 | 81.6  | 94.4  | 64:05 |
| root     | 79580 | 55.0  | 93.8  | 86:37 |

## Arquitetura inicial
A arquitetura definida inicialmente foi utilizar no projeto o estilo cliente/servidor onde o cliente seria o responsável pela leitura e escrita dos dados no disco enquanto o servidor ficaria responsável por separar os dados. O programa também iria possuir uma classe importante que é a classe *Processo*, cuja finalidade é armazenar os dados dos processos de cada usuário encontrado.

### O programa
Ao iniciar sua execução, o cliente abre o arquivo *pimpar* que se encontra no diretório *src/main/resources*. O programa então lê esse arquivo e armazena os dados lidos em um array, inicia o sistema de atores e envia esse array para o ator *Client*.

Recebido o array, são armazenadas as informações referentes ao tamanho dele a fim controlar quando parar o programa. Um loop então divide o array em n[^2] partes e envia essas partes para o(s) servidor(es).  

O servidor quando recebe os dados percorre cada linha separando cada user e armazenando os dados referentes a eles em uma lista de usuários onde cada usuário na lista é único. Finalizado esse processo, o servidor envia de volta ao cliente uma lista de usuários com os dados de seus processos.

Ao receber cada resultado do servidor o cliente guarda em uma lista os resultados recebidos e a cada x resultados grava os pids dos usuários em seus respectivos arquivos. Após receber todos os resultados os dados referentes aos gastos de cpu, memória e tempo são gravados nos arquivos e o sistema de atores é finalizado. Todos os arquivos são gravados no diretório *src/main/resources*.

### Estrutura do programa
##### Classes
<details>
<summary>Client extends Actor</summary>

	+ servidor:ActorSelection
	+ counter:Int
	+ tempoInicio:Long
	+ users:HashMap[String,(Array[Double],Set[Array[String]])]
	------------------------------------------------------------
	+ gravaUser():Unit
	+ gravaDados():Unit

</details>     

<details>
<summary>Server extends Actor</summary>

	+ users: Proccesso

</details>     

<details>
<summary>Processo</summary>

	- usuario: String
	- pid: Array[String]
	- cpu: Array[String]
	- memoria: Array[String]
	- tempo: Array[String]
	- mcpu: Array[Double]
	- mmemoria: Array[Double]
	- mtempo: Array[Double]
	------------------------------------------------------------
	+ calculaMedias(): Unit

</details>     

##### Componentes
* main(args: Array[String])
* Client extends Actor
* Server extends Actor
* Processo(usuario:String)

##### Conectores
* Start(conteudo: Array[String])
* Processa(conteudo: Array[String])
* Result(lista: Map[String,Processo])


## Problemas na implementação do projeto inicial
* **Transferência dos dados entre cliente e servidor**     
Inicialmente, pretendia-se transferir os dados utilizando a classe *Processo*, porém, resultava em um erro todas as vezes que se tentava enviar um objeto do tipo *Processo* como parâmetro das mensagens do akka. Dessa forma, uma solução temporária foi utilizar a estrutura `HashMap[String,(Array[Double],Set[Array[String]])]` para armazenar os resultados obtidos. Posteriormente a solução correta foi encontrada e foi possível transferir os dados utilizando uma instância da classe *Processo*.

* **Tempo**     
O tempo inicial foi afetado por conta da gravação no disco dos arquivos, pois, inicialmente, toda vez que o servidor retornava um resultado ele adicionava parte desse resultado em dois arquivos que guardam os pids. Assim, o tempo total estava demorando 20 segundos ou mais[^3]. 🐌

## Projeto implementado
Algumas partes do projeto foram modificadas. A arquitetura implementada difere da inicial em alguns aspectos importantes como a forma como os dados são transferidos entre o cliente e o servidor. O programa possui agora duas classes importantes que são a classe *Processo* e a classe *User*.

A finalidade da classe *Processo* mudou. Antes ela tinha a função de armazenar todos os processos do usuário, assim como suas médias de gastos. Agora a classe *User* possui a finalidade de armazenar o usuário, seus respectivos processos e a soma dos gastos dos processos. Também possui um método que realiza o cálculo das médias baseando-se nas somas dos gastos e quantidades de processos. A classe *Processo* refere-se agora a um único processo. Ela armazena seu pid, cpu gasta, memória gasta e tempo gasto.

Outro ponto modificado foi a eliminação das gravações entre um intervalo de x resultados recebidos. Dessa forma, o tempo de execução foi reduzido, pois, não serão feitos acessos desnecessários ao disco frequentemente. As gravações agora ocorrem somente quando todo o array de dados for percorrido.     

### O que mudou no programa
Ao iniciar sua execução, o programa cliente agora verifica se o diretório *out* existe. Caso não exista ele cria esse diretório. O objetivo de criar esse diretório é armazenar nele todos os resultados gerados de forma que os arquivos fiquem organizados.

Também foram feitas mudanças no sentido de corrigir os modificadores de acesso da Client cujos atributos se encontravam erroneamente como públicos.     

### Mudanças na estrutura do programa
##### Classes
<details>
<summary>Client extends Actor</summary>

	- servidor1:ActorSelection
	- servidor2:ActorSelection
	- users: Map[String,User]
	- counter:Int
	- fim: Int
	- tempoInicio:Long
	- len: Int
	- tam: Int
	------------------------------------------------------------
	+ gravaDados():Unit

</details>     

<details>
<summary>Server extends Actor</summary>
</details>     

<details>
<summary>Processo</summary>

	- pid: String
	- cpu: String
	- memoria: String
	- tempo: String

</details>   

<details>
<summary>User</summary>

	- username: String
	- processos: Array[Processo]
	- cpuGasta: Double
	- memGasta: Double
	- tempoGasto: Double
	------------------------------------------------------------
	+ addProcesso(proc: Processo):Unit
	+ addProcesso(pid: String, cpu: String, mem: String, tempo: String):Unit
	+ add(user:User):Unit
	+ addGastos(cpu: Double, mem: Double, tempo: Int):Unit
	+ calcularMedias():(Double,Double,Int)

</details>     

##### Componentes 
* main(args: Array[String])
* Client extends Actor
* Server extends Actor
* Processo(private[this] var pid: String, private[this] var cpu: String, private[this] var memoria: String, private[this] var tempo: String) extends Serializable
* User(private[this] var username: String, private[this] var processos: Array[Processo]) extends Serializable

##### Conectores  
* Start(conteudo: Array[String])
* Processa(conteudo: Array[String])
* Result(lista: Map[String,User])


### Resumo da implementação
**Cliente** 
1. [x] Abre o arquivo com os dados dos processos dos usuários
2. [x] Lê o arquivo e armazena os dados em um array
3. [x] Inicia o sistema de atores do cliente
2. [x] Divide o array em n partes
3. [x] Envia para o(s) servidor(es) cada parte
4. [ ] A cada x resultados recebidos acrescenta aos arquivos os pids
5. [x] Quando recebe todos os resultados, faz os calculos das médias e grava os arquivos

**Servidor**
1. [x] Inicia o sistema de atores do servidor
1. [x] Recebe os dados e os identifica os separando por usuário e armazenando seus respectivos processos.
2. [x] Envia para o cliente os dados devidamente separados por usuário

----------

## Documentação
A documentação foi gerada utilizando o comando:
```shell
$ sbt doc
```
Ela se encontra em:
* Cliente: doc/api/cliente/index.html
* Servidor: doc/api/servidor/index.html

----------

## Instalação
Caso não queira utilizar mais de uma máquina pule o guia de instalação.

### Pré-requisitos
* *scala 2.11.12*
* *sbt 1.2.3*
* *akka 2.3.8*

### Guia de instalação
Primeiramente deve-se clonar o projeto.
```shell
$ cd <diretorio-de-sua-escolha>
$ git clone https://gitlab.com/winy.dev/engsoft3-p1-impar.git
```

#### Instalando o cliente
Copie o diretório *cliente* para a máquina onde deseja executar o cliente. Pode renomeá-lo como desejar. Configure o arquivo *application.conf* que se encontra em *src/main/resources/*. Modifique o valor de *port* para a porta que desejar.

#### Instalando os servidores
Copie o diretório *servidor-1* para a máquina onde deseja executar o primeiro servidor. Pode renomeá-lo como desejar.     
Copie o diretório *servidor-2* para a máquina onde deseja executar o segundo servidor. Pode renomeá-lo como desejar.     

#### Configurando
Para modificar as portas utilizadas modifique o arquivo *application.conf* que se encontra em *src/main/resources/*. Modifique o valor de *port* para a porta que desejar. Modifique também o hostname.     

Deve-se modificar o arquivo *Cliente.scala* que se encontra em *src/main/scala/* alterando o endereço ip. Caso as portas dos servidores tenham sido modificadas altere-as também.     
```shell
private val servidor:ActorSelection = context.actorSelection("akka.tcp://Servidor@<ip>:<porta>/user/servidor");
```
Exemplo:
```shell
private val servidor1:ActorSelection = context.actorSelection("akka.tcp://Servidor@192.168.1.30:8080/user/servidor");
private val servidor2:ActorSelection = context.actorSelection("akka.tcp://Servidor@192.168.1.35:8081/user/servidor");
```

### Executando   
Para rodar o servidor ou o cliente vá para o diretório principal onde se encontra o arquivo *build.sbt* e execute:
```shell
$ sbt run
```


---------------------------------------------
📎

[^1]: Resultado do comando `ps -aux` no linux. 
[^2]: Note que o número de partes e o número de servidores poderão afetar a performance alterando o tempo total que levará para gerar os resultados esperados.  
[^3]: O tempo depende do hardware da máquina utilizada. Pode varia para mais ou para menos.   